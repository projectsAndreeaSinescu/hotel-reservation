<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div style="padding: 5px;">
	<ul>
		<li><a href="${pageContext.request.contextPath}/">Home</a></li>
		<li><a href="${pageContext.request.contextPath}/login">Login</a></li>
		<li><a href="${pageContext.request.contextPath}/userInfo">Informatii cont</a></li>
		<li><a href="${pageContext.request.contextPath}/cameraList">Lista camere</a></li>
		<li><a href="${pageContext.request.contextPath}/userList">Lista useri</a></li>
		<li><a href="${pageContext.request.contextPath}/cautare">Cautare camera</a></li>
	</ul>
</div>
