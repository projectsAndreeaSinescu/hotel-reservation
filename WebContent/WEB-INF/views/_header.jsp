<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<div style="background: white; height: 45px; padding: 5px;">
  <div style="float: left">
     <p>My Site</p>
  </div>
 
  <div style="float: right; padding: 10px; text-align: right;">
 
     <!-- User store in session with attribute: loginedUser -->
     Buna <b>${loginedUser.userName}</b>
     <br/>
     <a href="${pageContext.request.contextPath}/logout">Logout</a>
   <br/>
    
  </div>
 
</div>