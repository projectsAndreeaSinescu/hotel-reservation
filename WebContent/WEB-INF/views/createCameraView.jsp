<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Creare Camera</title>
<style>
html {
	font-size: 16px;
}

body {
	background-repeat: no-repeat;
	background-size: cover;
	background-color: #f9b79d;
}

h1, h2 {
	color: darkblue;
	margin: auto;
	width: 50%;
	/*padding: 1%;*/
	text-align: center;
	font-size: 1.1em;
	font-stretch: ultra-expanded;
}

h1 {
	font-size: 2em;
	padding: 2%;
}

/*MENIU*/
ul {
	float: left;
	height: auto;
	width: 100%;
	margin-right: 2%;
	margin-left: 2%;
	margin-bottom: 2%;
}

ul>li {
	font-size: 1.2rem;
	font-weight: bold;
	height: 2rem;
	text-align: center;
	line-height: 2rem;
	list-style-type: none;
	overflow: hidden;
	float: left;
	width: 15%;
	background: #EDE2CA;
	border-radius: 1em;
	z-index: 30;
}

ul>li>a {
	display: block;
	height: 100%;
	text-decoration: none;
	color: black;
}

ul>li>a:hover {
	color: darkred;
}

ul>li:hover {
	overflow: visible;
	opacity: 1;
	z-index: 40;
	padding-left: 0;
}


/*TABLE*/
table, table td, table th {
	font-size: 1rem;
	border: 0.01rem solid black;
	text-align: center;
}

table th {
	font-size: 1.1em;
	padding-left: 1%;
	padding-right: 1%;
}

table td {
	padding: 0.5%;
	font-size: 1.1em;
	padding: 1%;
}

table {
	/*align-self:center;*/
	border-spacing: 0;
	border-collapse: separate;
	box-sizing: border-box;
	width: 90%;
	float: scroll;
	margin: 5%;
	background-color: #DCD6CA;
	opacity: 0.85;
}

/*LINKuri*/
a.link {
	color: blue;
	text-decoration: none;
}

a.link:hover {
	color: darkred;
}

a#link-titlu {
	text-decoration: none;
	color: darkblue;
}

/*FOOTER*/
footer {
	background-color: white;
	width: 100%;
	opacity: 0.8;
}

header {
	width: 100%;
}

footer>p, footer>address {
	box-sizing: border-box;
	width: 50%;
	float: left;
	text-align: center;
}

footer>address {
	margin-top: 1%;
	margin-bottom: 1%;
}
</style>
</head>
<body>
	<c:if test="${loginedUser.userTip=='admin'}">
		<jsp:include page="_header.jsp"></jsp:include>
		<jsp:include page="_menuAdmin.jsp"></jsp:include>

		<h1>Creare Camera</h1>

		<p style="color: red;">${errorString}</p>

		<form method="POST"
			action="${pageContext.request.contextPath}/createCamera">
			<table border="0">
				<tr>
					<td>Numar</td>
					<td><input type="text" name="numar" value="${camera.numar}" /></td>
				</tr>
				<tr>
					<td>Etaj</td>
					<td><input type="text" name="etaj" value="${camera.etaj}" /></td>
				</tr>
				<tr>
					<td>Tip</td>
					<td><input type="text" name="tip" value="${camera.tip}" /></td>
				</tr>
				<tr>
					<td>Pret</td>
					<td><input type="text" name="pret" value="${camera.pret}" /></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><input type="text" name="status" value="${camera.status}" /></td>
				</tr>
				<tr>
					<td>Aer conditionat</td>
					<td><input type="text" name="extra1" value="${camera.extra1}" /></td>
				</tr>
				<tr>
					<td>Minibar</td>
					<td><input type="text" name="extra2" value="${camera.extra2}" /></td>
				</tr>
				<tr>
					<td>Cafetiera</td>
					<td><input type="text" name="extra3" value="${camera.extra3}" /></td>
				</tr>
				<tr>
					<td>Uscator</td>
					<td><input type="text" name="extra4" value="${camera.extra4}" /></td>
				</tr>
				<tr>
					<td>Parcare</td>
					<td><input type="text" name="extra5" value="${camera.extra5}" /></td>
				</tr>
				<tr>
					<td>Balcon</td>
					<td><input type="text" name="extra6" value="${camera.extra6}" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /> <a
						href="cameraList">Cancel</a></td>
				</tr>
			</table>
		</form>

		<jsp:include page="_footer.jsp"></jsp:include>
	</c:if>
</body>
</html>