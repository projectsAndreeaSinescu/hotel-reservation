package org.o7planning.proiect.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.o7planning.proiect.beans.Camera;
import org.o7planning.proiect.utils.DBUtils;
import org.o7planning.proiect.utils.MyUtils;

@WebServlet("/cautare")
public class CautareServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public CautareServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/cautareView.jsp");
        dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);
		String etaj;
		String tip;
		String extra1;
		if(request.getParameter("etaj").isEmpty()!=true)
			 etaj = (String) request.getParameter("etaj");
		else etaj = null;
		if(request.getParameter("tip").isEmpty()!=true)
			 tip = (String) request.getParameter("tip");
		else tip = null;
		if(request.getParameter("extra1").isEmpty()!=true)
			extra1 = (String) request.getParameter("extra1");
		else extra1 = null;
		String extra2;
		if(request.getParameter("extra2").isEmpty()!=true)
			extra2 = (String) request.getParameter("extra2");
		else extra2 = null;
		String extra3;
		if(request.getParameter("extra3").isEmpty()!=true)
			extra3 = (String) request.getParameter("extra3");
		else extra3 = null;
		String extra4;
		if(request.getParameter("extra4").isEmpty()!=true)
			extra4 = (String) request.getParameter("extra4");
		else extra4 = null;
		String extra5;
		if(request.getParameter("extra5").isEmpty()!=true)
			extra5 = (String) request.getParameter("extra5");
		else extra5 = null;
		String extra6;
		if(request.getParameter("extra6").isEmpty()!=true)
			extra6 = (String) request.getParameter("extra6");
		else extra6 = null;
		Float pret;
		if(request.getParameter("pret").isEmpty()!=true)
              pret = Float.parseFloat(request.getParameter("pret"));
		else pret = (float) -1;
        Camera camera = null;
        
        String errorString = null;
        List<Camera> list = null;
        try {
            list = DBUtils.findCamera(conn,etaj,tip,pret,extra1,extra2,extra3,extra4,extra5,extra6);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        
        request.setAttribute("errorString", errorString);
        request.setAttribute("cautareList", list);
 
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/cautareView.jsp");
            dispatcher.forward(request, response);
        }
        else {
        	if(list==null || list.isEmpty())
        		request.setAttribute("nimic", "da");
        	else request.setAttribute("nimic", "nu");
        	RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/cautareListView.jsp");
            dispatcher.forward(request, response);
	    }
	}
}
