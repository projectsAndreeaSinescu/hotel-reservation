package org.o7planning.proiect.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.o7planning.proiect.beans.Camera;
import org.o7planning.proiect.utils.DBUtils;
import org.o7planning.proiect.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/editCamera" })
public class EditCameraServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public EditCameraServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        Float id = Float.parseFloat(request.getParameter("id"));
 
        Camera camera = null;
 
        String errorString = null;
 
        try {
            camera = DBUtils.findCamera(conn, id);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
 
        if (errorString != null && camera == null) {
            response.sendRedirect(request.getServletPath() + "/cameraList");
            return;
        }
 
        request.setAttribute("errorString", errorString);
        request.setAttribute("camera", camera);
 
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/editCameraView.jsp");
        dispatcher.forward(request, response);
 
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        String numar = (String) request.getParameter("numar");
        String etaj = (String) request.getParameter("etaj");
        String tip = (String) request.getParameter("tip");
        Float pret = Float.parseFloat(request.getParameter("pret"));
        String status = (String) request.getParameter("status");
        String extra1 = (String) request.getParameter("extra1");
        String extra2 = (String) request.getParameter("extra2");
        String extra3 = (String) request.getParameter("extra3");
        String extra4 = (String) request.getParameter("extra4");
        String extra5 = (String) request.getParameter("extra5");
        String extra6 = (String) request.getParameter("extra6");
   
        Camera camera = new Camera(numar,etaj,tip,pret,status,extra1,extra2,extra3,extra4,extra5,extra6);
 
        String errorString = null;
 
        try {
            DBUtils.updateCamera(conn, camera);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        request.setAttribute("errorString", errorString);
        request.setAttribute("camera", camera);
 
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/editCameraView.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/cameraList");
        }
    }
 
}
