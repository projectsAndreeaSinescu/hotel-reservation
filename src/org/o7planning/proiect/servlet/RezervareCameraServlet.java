package org.o7planning.proiect.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.o7planning.proiect.beans.Camera;
import org.o7planning.proiect.beans.Rezervare;
import org.o7planning.proiect.utils.DBUtils;
import org.o7planning.proiect.utils.MyUtils;

 
@WebServlet(urlPatterns = { "/rezervareCamera" })
public class RezervareCameraServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public RezervareCameraServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        Float id = Float.parseFloat(request.getParameter("id"));
 
        Camera camera = null;
        Rezervare rezervare = null;
        String errorString = null;
 
        try {
            camera = DBUtils.findCamera(conn, id);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
 
        if (errorString != null && camera == null) {
            response.sendRedirect(request.getServletPath() + "/rezervareList");
            return;
        }
 
        request.setAttribute("errorString", errorString);
        request.setAttribute("camera", camera);
        request.setAttribute("rezervare", rezervare);
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/rezervareCameraView.jsp");
        dispatcher.forward(request, response);
 
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	Connection conn = MyUtils.getStoredConnection(request);
		float id = Float.valueOf(request.getParameter("id"));
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		Date data1;
		Date data2;
		Rezervare rez = new Rezervare();
		String errorString = null;
		
		try {
			rez.setIdCamera(id);
			rez.setDataInceput(format.parse(request.getParameter("dataCheckIn")));
			rez.setDataSfarsit(format2.parse(request.getParameter("dataCheckOut")));
			rez.setPretTotal(Float.parseFloat(request.getParameter("pretTotal")));
			Rezervare noua = DBUtils.checkDispon(conn, rez);
			if(noua == null)
			{
				errorString = "Nu este libera camera atunci!";
			}
			else DBUtils.rezervaCamera(conn, rez);

		} catch (java.text.ParseException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Camera camera = new Camera();
		camera.setId(id);
		request.setAttribute("errorString", errorString);
        request.setAttribute("camera", camera);
	    if (errorString != null) {
	        RequestDispatcher dispatcher = request.getServletContext()
	                    .getRequestDispatcher("/WEB-INF/views/rezervareCameraView.jsp");
	        
	        dispatcher.forward(request, response);
	    }
	    else {
	        response.sendRedirect(request.getContextPath() + "/rezervareList");
	    }
    }
}
