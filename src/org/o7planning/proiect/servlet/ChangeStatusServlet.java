package org.o7planning.proiect.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.o7planning.proiect.beans.Camera;
import org.o7planning.proiect.utils.DBUtils;
import org.o7planning.proiect.utils.MyUtils;


@WebServlet(urlPatterns = { "/changeStatus" })
public class ChangeStatusServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	 
    public ChangeStatusServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        Float id = Float.parseFloat(request.getParameter("id"));
 
        Camera camera = null;
 
        String errorString = null;
 
        try {
            camera = DBUtils.findCamera(conn, id);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
 
        if (errorString != null && camera == null) {
            response.sendRedirect(request.getServletPath() + "/cameraList");
            return;
        }
 
        request.setAttribute("errorString", errorString);
        request.setAttribute("camera", camera);
 
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/changeStatusView.jsp");
        dispatcher.forward(request, response);
 
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        String id = (String) request.getParameter("id");
        String status = (String) request.getParameter("status");
   
        Camera camera = new Camera();
        camera.setId(Float.parseFloat(id));
        camera.setStatus(status);
 
        String errorString = null;
 
        try {
            DBUtils.updateStatus(conn, camera); 
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        request.setAttribute("errorString", errorString);
        request.setAttribute("camera", camera);
 
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/changeStatusView.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/cameraList");
        }
    }
 

}
