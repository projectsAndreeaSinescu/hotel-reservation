package org.o7planning.proiect.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.o7planning.proiect.beans.ContUser;
import org.o7planning.proiect.utils.DBUtils;
import org.o7planning.proiect.utils.MyUtils;
 
@WebServlet(urlPatterns = { "/createUser" })
public class CreateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public CreateUserServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/createUserView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        String userName = (String) request.getParameter("userName");
        String password = (String) request.getParameter("password");
        String userTip = (String) request.getParameter("userTip");
        
        ContUser user = new ContUser(userName,userTip,password);
 
        String errorString = null;
 
        String regex = "\\w+";
 
        if (userName == null || password == null || userTip == null || !userName.matches(regex) || !userTip.matches(regex) || !password.matches(regex)) {
            errorString = "User invalid!";
        }
 
        if (errorString == null) {
            try {
                DBUtils.insertUser(conn, user);
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            }
        }
 
        request.setAttribute("errorString", errorString);
        request.setAttribute("user", user);
 
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/createUserView.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/userList");
        }
    }
 
}
