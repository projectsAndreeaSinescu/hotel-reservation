package org.o7planning.proiect.beans;

import java.util.Date;

public class Rezervare {
	private String codRez;
	private float idCamera;
	private Date dataInceput;
	private Date dataSfarsit;
	private float pretTotal;
	private static Integer codAuto=100;
	
	public Rezervare() {
		
	}
	
	
	public static Integer generateCod() {return codAuto++; };
	
	public Rezervare(String cod, float id, Date data_inceput, Date data_sfarsit) {
		super();
		this.codRez = cod;
		this.idCamera = id;
		this.dataInceput = data_inceput;
		this.dataSfarsit = data_sfarsit;
	}
	
	public String getCodRez() {
		return codRez;
	}
	
	public void setCodRez() {
		this.codRez = Rezervare.generateCod().toString();
	}
	
	public float getIdCamera() {
		return idCamera;
	}
	
	public void setIdCamera(float id) {
		idCamera = id;
	}
	
	public Date getDataInceput() {
		return dataInceput;
	}
	
	public void setDataInceput(Date data_inceput) {
		this.dataInceput = data_inceput;
	}
	
	public Date getDataSfarsit() {
		return dataSfarsit;
	}
	
	public void setDataSfarsit(Date dataSfarsit) {
		this.dataSfarsit = dataSfarsit;
	}
	
	public float getPretTotal() {
		return pretTotal;
	}
	
	public void setPretTotal(float pret_total) {
		this.pretTotal = pret_total;
	}	
}
