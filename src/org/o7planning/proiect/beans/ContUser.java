package org.o7planning.proiect.beans;

public class ContUser {
	private String userName;
	private String userTip;
	private String password;
	
	public ContUser() {
	
	}
	public ContUser(String userName, String userTip, String password) {
		this.userName = userName;
		this.userTip = userTip;
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserTip() {
		return userTip;
	}
	public void setUserTip(String userTip) {
		this.userTip = userTip;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
