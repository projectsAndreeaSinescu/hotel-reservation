package org.o7planning.proiect.beans;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

public class Camera {
	private float id;
	private String numar;
	private String etaj;
	private String tip;
	private float pret;
	private String status;
	private String extra1;
	private String extra2;
	private String extra3;
	private String extra4;
	private String extra5;
	private String extra6;

	public Camera() {

	}

	public Camera(String numar, String etaj, String tip, float pret, String status, String extra1, String extra2,
			String cafe, String extra4, String extra5, String extra6) {
		super();
		this.id = Float.valueOf(numar) + 100* Float.valueOf(etaj);
		this.numar = numar;
		this.etaj = etaj;
		this.tip = tip;
		this.pret = pret;
		this.status = status;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = cafe;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.extra6 = extra6;
	}

	public float getId() {
		return id;
	}

	public void setId(float id) {
		this.id = id;
	}

	public String getNumar() {
		return numar;
	}

	public void setNumar(String numar) {
		this.numar = numar;
	}

	public String getEtaj() {
		return etaj;
	}

	public void setEtaj(String etaj) {
		this.etaj = etaj;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public float getPret() {
		return pret;
	}

	public void setPret(float pret) {
		this.pret = pret;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getExtra2() {
		return extra2;
	}

	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getExtra3() {
		return extra3;
	}

	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}

	public String getExtra4() {
		return extra4;
	}

	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}

	public String getExtra5() {
		return extra5;
	}

	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}

	public String getExtra6() {
		return extra6;
	}

	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}

}