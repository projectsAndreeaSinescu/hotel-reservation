package org.o7planning.proiect.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.o7planning.proiect.beans.Camera;
import org.o7planning.proiect.beans.ContUser;
import org.o7planning.proiect.beans.Rezervare;

public class DBUtils {

	public static ContUser findUser(Connection conn, String userName, String password) throws SQLException {

		String sql = "Select a.User_Nume, a.parola, a.User_Tip from Cont_User a "
				+ " where a.User_Nume = ? and a.parola= ?";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setString(1, userName);
			pstm.setString(2, password);
			try (ResultSet rs = pstm.executeQuery();) {
				if (rs.next()) {
					String userTip = rs.getString("User_Tip");
					ContUser user = new ContUser();
					user.setUserName(userName);
					user.setPassword(password);
					user.setUserTip(userTip);
					return user;
				}
			}
		}
		return null;
	}

	public static ContUser findUser(Connection conn, String userName) throws SQLException {

		String sql = "Select a.User_Nume, a.Parola, a.User_Tip from Cont_User a where a.User_Nume = ?";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setString(1, userName);
			try (ResultSet rs = pstm.executeQuery();) {
				if (rs.next()) {
					String password = rs.getString("Parola");
					String userTip = rs.getString("User_Tip");
					ContUser user = new ContUser();
					user.setUserName(userName);
					user.setPassword(password);
					user.setUserTip(userTip);
					return user;
				}
			}
		}
		return null;
	}

	public static List<Camera> queryCamera(Connection conn) throws SQLException {
		String sql = "Select a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, a.Balcon from Camera a ";

		try (PreparedStatement pstm = conn.prepareStatement(sql); ResultSet rs = pstm.executeQuery();) {
			List<Camera> list = new ArrayList<Camera>();
			while (rs.next()) {
				String numar = rs.getString("Numar");
				String etaj = rs.getString("Etaj");
				String tip = rs.getString("Tip");
				float pret = rs.getFloat("Pret");
				String status = rs.getString("Status");
				String extra1 = rs.getString("Aer_Cond");
				String extra2 = rs.getString("Minibar");
				String extra3 = rs.getString("Cafetiera");
				String extra4 = rs.getString("Uscator");
				String extra5 = rs.getString("Parcare");
				String extra6 = rs.getString("Balcon");
				Camera cam = new Camera(numar, etaj, tip, pret, status, extra1, extra2, extra3, extra4, extra5, extra6);
				list.add(cam);
			}
			return list;
		}
	}

	public static List<Rezervare> queryRezervare(Connection conn) throws SQLException {
		String sql = "Select a.cod, a.cod_camera, a.data_inceput, a.data_sfarsit, a.pret_total from Rezervare a ";

		try (PreparedStatement pstm = conn.prepareStatement(sql); ResultSet rs = pstm.executeQuery();) {
			List<Rezervare> list = new ArrayList<Rezervare>();
			while (rs.next()) {
				String cod = rs.getString("cod");
				float idCamera = rs.getFloat("cod_camera");
				Date dataInceput = rs.getDate("data_inceput");
				Date dataSfarsit = rs.getDate("data_sfarsit");
				float pret = rs.getFloat("pret_total");
				Rezervare rezervare = new Rezervare(cod, idCamera, dataInceput, dataSfarsit);
				rezervare.setPretTotal(pret);
				list.add(rezervare);
			}
			return list;
		}
	}

	public static List<ContUser> queryUser(Connection conn) throws SQLException {
		String sql = "Select a.User_Nume, a.parola, a.User_Tip from Cont_User a ";

		try (PreparedStatement pstm = conn.prepareStatement(sql); ResultSet rs = pstm.executeQuery();) {
			List<ContUser> list = new ArrayList<ContUser>();
			while (rs.next()) {
				String userName = rs.getString("User_Nume");
				String password = rs.getString("Parola");
				String userTip = rs.getString("User_Tip");
				ContUser user = new ContUser(userName, userTip, password);
				list.add(user);
			}
			return list;
		}
	}

	public static Camera findCamera(Connection conn, Float id) throws SQLException {
		String sql = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where a.id = ?";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setFloat(1, id);
			try (ResultSet rs = pstm.executeQuery();) {
				while (rs.next()) {
					String numar = rs.getString("Numar");
					String etaj = rs.getString("Etaj");
					String tip = rs.getString("Tip");
					String status = rs.getString("Status");
					String extra1 = rs.getString("Aer_Cond");
					String extra2 = rs.getString("Minibar");
					String extra3 = rs.getString("Cafetiera");
					String extra4 = rs.getString("Uscator");
					String extra5 = rs.getString("Parcare");
					String extra6 = rs.getString("Balcon");
					float pret = rs.getFloat("Pret");
					Camera cam = new Camera(numar, etaj, tip, pret, status, extra1, extra2, extra3, extra4, extra5,
							extra6);
					return cam;
				}
			}
		}
		return null;
	}

	public static List<Camera> findCamera(Connection conn, String etaj, String tip, Float pret, String extra1,
			String extra2, String extra3, String extra4, String extra5, String extra6) throws SQLException {
		String sql1 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where a.etaj = ?";
		String sql2 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where a.tip = ? ";
		String sql3 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where  a.pret=?";
		String sql4 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where  a.aer_cond = ? ";
		String sql5 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where  a.minibar = ? ";
		String sql6 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where  a.cafetiera = ?  ";
		String sql7 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where a.uscator = ? ";
		String sql8 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where a.parcare = ? ";
		String sql9 = "Select a.Id, a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, "
				+ "a.Balcon from Camera a where a.balcon = ? ";
		String sql = "";
		int nr = 0;
		int[] vector = new int[10];
		if (etaj != null) {
			vector[0] = 1;
			if (nr == 0) {
				sql = sql + sql1;
				nr++;

				//Sinescu Andreea 9
				//Eftene Andra 9
				
				
			} else {
				sql = sql + " intersect " + sql1;
			}
		} else
			vector[0] = 0;
		if (tip != null) {
			vector[1] = 1;
			if (nr == 0) {
				sql = sql + sql2;
				nr++;
			} else
				sql = sql + " intersect " + sql2;
		} else
			vector[1] = 0;
		if (pret != (float) -1) {
			vector[2] = 1;
			if (nr == 0) {
				sql = sql + sql3;
				nr++;
			} else
				sql = sql + " intersect " + sql3;
		} else
			vector[2] = 0;
		if (extra1 != null) {
			vector[3] = 1;
			if (nr == 0) {
				sql = sql + sql4;
				nr++;
			} else
				sql = sql + " intersect " + sql4;
		} else
			vector[3] = 0;
		if (extra2 != null) {
			vector[4] = 1;
			if (nr == 0) {
				sql = sql + sql5;
				nr++;
			} else
				sql = sql + " intersect " + sql5;
		} else
			vector[4] = 0;
		if (extra3 != null) {
			vector[5] = 1;
			if (nr == 0) {
				sql = sql + sql6;
				nr++;
			} else
				sql = sql + " intersect " + sql6;
		} else
			vector[5] = 0;
		if (extra4 != null) {
			vector[6] = 1;
			if (nr == 0) {
				sql = sql + sql7;
				nr++;
			} else
				sql = sql + " intersect " + sql7;
		} else
			vector[6] = 0;
		if (extra5 != null) {
			vector[7] = 1;
			if (nr == 0) {
				sql = sql + sql8;
				nr++;
			} else
				sql = sql + " intersect " + sql8;
		} else
			vector[7] = 0;
		if (extra6 != null) {
			vector[8] = 1;
			if (nr == 0) {
				sql = sql + sql9;
				nr++;
			} else
				sql = sql + " intersect " + sql9;
		} else
			vector[8] = 0;
		if (nr == 0)
			return null;
		nr = 1;
		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			if (vector[0] == 1) {
				pstm.setString(nr, etaj);
				nr++;
			}
			if (vector[1] == 1) {
				pstm.setString(nr, tip);
				nr++;
			}
			if (vector[2] == 1) {
				pstm.setFloat(nr, pret);
				nr++;
			}
			if (vector[3] == 1) {
				pstm.setString(nr, extra1);
				nr++;
			}
			if (vector[4] == 1) {
				pstm.setString(nr, extra2);
				nr++;
			}
			if (vector[5] == 1) {
				pstm.setString(nr, extra3);
				nr++;
			}
			if (vector[6] == 1) {
				pstm.setString(nr, extra4);
				nr++;
			}
			if (vector[7] == 1) {
				pstm.setString(nr, extra5);
				nr++;
			}
			if (vector[8] == 1) {
				pstm.setString(nr, extra6);
				nr++;
			}
			try (ResultSet rs = pstm.executeQuery();) {
				List<Camera> list = new ArrayList<Camera>();
				if (rs.next()) {
					if (rs.getString("Status").equals("liber")) {
						String numar = rs.getString("Numar");
						etaj = rs.getString("Etaj");
						tip = rs.getString("Tip");
						String status = rs.getString("Status");
						extra1 = rs.getString("Aer_Cond");
						extra2 = rs.getString("Minibar");
						extra3 = rs.getString("Cafetiera");
						extra4 = rs.getString("Uscator");
						extra5 = rs.getString("Parcare");
						extra6 = rs.getString("Balcon");
						pret = rs.getFloat("Pret");
						Camera cam = new Camera(numar, etaj, tip, pret, status, extra1, extra2, extra3, extra4, extra5,
								extra6);
						list.add(cam);
					}
					while (rs.next()) {
						if (rs.getString("Status").equals("liber")) {
							String numar = rs.getString("Numar");
							etaj = rs.getString("Etaj");
							tip = rs.getString("Tip");
							String status = rs.getString("Status");
							extra1 = rs.getString("Aer_Cond");
							extra2 = rs.getString("Minibar");
							extra3 = rs.getString("Cafetiera");
							extra4 = rs.getString("Uscator");
							extra5 = rs.getString("Parcare");
							extra6 = rs.getString("Balcon");
							pret = rs.getFloat("Pret");
							Camera cam2 = new Camera(numar, etaj, tip, pret, status, extra1, extra2, extra3, extra4,
									extra5, extra6);
							list.add(cam2);
						}
					}
				}
				return list;
			}
		}
	}

	public static void updateCamera(Connection conn, Camera camera) throws SQLException {
		String sql = "Update Camera set Numar = ?, Etaj = ?, Tip = ?, Pret = ?, Status=?, Aer_Cond = ?, Minibar=?, Cafetiera = ?, Uscator = ?, Parcare = ?,Balcon = ? where Id = ?";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setString(1, camera.getNumar());
			pstm.setString(2, camera.getEtaj());
			pstm.setString(3, camera.getTip());
			pstm.setFloat(4, camera.getPret());
			pstm.setString(5, camera.getStatus());
			pstm.setString(6, camera.getExtra1());
			pstm.setString(7, camera.getExtra2());
			pstm.setString(8, camera.getExtra3());
			pstm.setString(9, camera.getExtra4());
			pstm.setString(10, camera.getExtra5());
			pstm.setString(11, camera.getExtra6());
			pstm.setFloat(12, camera.getId());
			pstm.executeUpdate();
		}
	}

	public static void updateUser(Connection conn, ContUser user) throws SQLException {
		String sql = "Update Cont_User set User_Tip = ?, Parola = ? where User_Nume = ?";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setString(1, user.getUserTip());
			pstm.setString(2, user.getPassword());
			pstm.setString(3, user.getUserName());
			pstm.executeUpdate();
		}
	}

	public static void insertCamera(Connection conn, Camera camera) throws SQLException {
		String sql = "Insert into Camera(Id,Numar, Etaj, Tip, Pret, Status, Aer_Cond, Minibar, Cafetiera, Uscator, Parcare, Balcon) "
				+ "values (?,?,?,?,?,?,?,?,?,?,?,?)";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setFloat(1, camera.getId());
			pstm.setString(2, camera.getNumar());
			pstm.setString(3, camera.getEtaj());
			pstm.setString(4, camera.getTip());
			pstm.setFloat(5, camera.getPret());
			if (camera.getStatus() != "")
				pstm.setString(6, camera.getStatus());
			else
				pstm.setString(6, "liber");
			if (camera.getExtra1() != "")
				pstm.setString(7, camera.getExtra1());
			else
				pstm.setString(7, "nu");
			if (camera.getExtra2() != "")
				pstm.setString(8, camera.getExtra2());
			else
				pstm.setString(8, "nu");
			if (camera.getExtra3() != "")
				pstm.setString(9, camera.getExtra3());
			else
				pstm.setString(9, "nu");
			if (camera.getExtra4() != "")
				pstm.setString(10, camera.getExtra4());
			else
				pstm.setString(10, "nu");
			if (camera.getExtra5() != "")
				pstm.setString(11, camera.getExtra5());
			else
				pstm.setString(11, "nu");
			if (camera.getExtra6() != "")
				pstm.setString(12, camera.getExtra6());
			else
				pstm.setString(12, "nu");
			pstm.executeUpdate();
		}
	}

	public static void insertUser(Connection conn, ContUser user) throws SQLException {
		String sql = "Insert into Cont_User(User_Nume,User_Tip,Parola) values (?,?,?)";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setString(1, user.getUserName());
			pstm.setString(2, user.getUserTip());
			pstm.setString(3, user.getPassword());
			pstm.executeUpdate();
		}
	}

	public static void deleteCamera(Connection conn, Float id) throws SQLException {
		String sql = "Delete From Camera where Id = ?";

		try (PreparedStatement pstm = conn.prepareStatement(sql);) {
			pstm.setFloat(1, id);
			pstm.executeUpdate();
		}
	}

	public static Rezervare checkDispon(Connection conn, Rezervare rez) throws SQLException {
		String sql = "select * from rezervare where ((data_inceput between ? and ?) or (data_sfarsit between ? and ?) or (? between data_inceput and data_sfarsit) or (? between data_inceput and data_sfarsit)) and cod_camera = ?";
		try (PreparedStatement ps = conn.prepareStatement(sql);) {
			ps.setDate(1, new java.sql.Date(rez.getDataInceput().getTime()));
			ps.setDate(2, new java.sql.Date(rez.getDataSfarsit().getTime()));
			ps.setDate(3, new java.sql.Date(rez.getDataInceput().getTime()));
			ps.setDate(4, new java.sql.Date(rez.getDataSfarsit().getTime()));
			ps.setDate(5, new java.sql.Date(rez.getDataInceput().getTime()));
			ps.setDate(6, new java.sql.Date(rez.getDataSfarsit().getTime()));
			ps.setFloat(7, rez.getIdCamera());
			ps.executeUpdate();

			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return null;
				} else
					return rez;
			}
		}
	}

	public static void rezervaCamera(Connection conn, Rezervare rezervare) throws SQLException {
		String sql = "insert into rezervare values (?, ?, ?, ?, ?)";
		String sql2 = "select MAX(cod) AS cod from rezervare";
		String cod = null;
		try (PreparedStatement ps = conn.prepareStatement(sql2); ResultSet rs = ps.executeQuery();) {
			if (rs.next())
				cod = rs.getString("cod");
		}
		int cod1 = Integer.valueOf(cod);
		cod1++;
		cod = String.valueOf(cod1);
		try (PreparedStatement ps = conn.prepareStatement(sql);) {
			ps.setString(1, cod);
			ps.setFloat(2, rezervare.getIdCamera());
			ps.setDate(3, new java.sql.Date(rezervare.getDataInceput().getTime()));
			ps.setDate(4, new java.sql.Date(rezervare.getDataSfarsit().getTime()));
			ps.setFloat(5, rezervare.getPretTotal());
			ps.executeUpdate();
		}
	}

	public static void updateStatus(Connection conn, Camera camera) throws SQLException {

		if (camera.getStatus().equals("liber")) {
			String sql = "Update Camera set Status=? where id = ? and status = 'liber, necuratat'";

			try (PreparedStatement pstm = conn.prepareStatement(sql);) {
				pstm.setString(1, camera.getStatus());
				pstm.setFloat(2, camera.getId());
				pstm.executeUpdate();
			}
		}

		String sql2 = "select MAX(ID) AS cod from mesaj";
		String cod = null;
		try (PreparedStatement ps = conn.prepareStatement(sql2); ResultSet rs = ps.executeQuery();) {
			if (rs.next())
				cod = rs.getString("cod");
		}
		int cod1 = Integer.valueOf(cod);
		cod1++;
		cod = String.valueOf(cod1);

		String sql3 = "insert into mesaj(ID, TEXT, COD_CAMERA) values(?,?,?) ";
		try (PreparedStatement ps = conn.prepareStatement(sql3);) {
			ps.setString(1, cod);
			ps.setString(2, camera.getStatus());
			ps.setFloat(3, camera.getId());
			ps.executeUpdate();
		}
	}

	public static List<Camera> userCamera(Connection conn) throws SQLException {
		String sql = "Select a.Numar, a.Etaj, a.Tip, a.Pret, a.Status, a.Aer_Cond, a.Minibar, a.Cafetiera, a.Uscator, a.Parcare, a.Balcon from Camera a where a.status = 'liber'";

		try (PreparedStatement pstm = conn.prepareStatement(sql); ResultSet rs = pstm.executeQuery();) {
			List<Camera> list = new ArrayList<Camera>();
			while (rs.next()) {
				String numar = rs.getString("Numar");
				String etaj = rs.getString("Etaj");
				String tip = rs.getString("Tip");
				float pret = rs.getFloat("Pret");
				String status = rs.getString("Status");
				String extra1 = rs.getString("Aer_Cond");
				String extra2 = rs.getString("Minibar");
				String extra3 = rs.getString("Cafetiera");
				String extra4 = rs.getString("Uscator");
				String extra5 = rs.getString("Parcare");
				String extra6 = rs.getString("Balcon");
				Camera cam = new Camera(numar, etaj, tip, pret, status, extra1, extra2, extra3, extra4, extra5, extra6);
				list.add(cam);
			}
			return list;
		}
	}

}
